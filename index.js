const readline = require("readline-sync");
let n;
let playersArray = [];
function inputTaking() {
  n = Number(readline.question("PLEASE ENTER NUMBER OF PLAYERS:- "));
  if (isNaN(n)) {
    console.log("\nPLEASE ENTER A VALID NUMBER");
    inputTaking();
  }
 
  let i = 0;

  while (i < n) {
    let player = readline.question("ENTER NAME OF PLAYER:- ");
    playersArray.push(player.toUpperCase());
    i++;
  }

  i = 0;
  console.log("PLAYERS ARE -");
  while (i < n) {
    console.log(playersArray[i] + ", PLAYER ID IS- " + i);
    i++;
  }

  choosePlayer();
}

inputTaking();

function choosePlayer() {
  let id = readline.question(
    "PLEASE SPECIFY WHICH PLAYER WANTS TO MAKE GUESS FIRST ENTER ID:- "
  );
  playerInput(id);
}

function playerInput(id) {
  if (id >= 0 && id < n) {
    player(id);
  } else {
    console.log("\nPLEASE ENTER A VALID ID");
    choosePlayer();
  }
}

function player(id) {
  console.log("TO EXIT PRESS 0");
  let word = readline.question(
    `${playersArray[id]} PLEASE ENTER A WORD FOR OTHERS:- `
  );
  console.clear();
  if (word == 0) process.exit();
  let i = 0;
  while (i < 6) {
    playersArray.forEach((item, index) => {
      if (index == id) {
      } else {
        let guessedWord = readline.question(`${item} PLEASE MAKE A GUESS:-`);

        if (guessedWord == word) {
          console.log(`\nCONGRATULATIONS ${item} 💐! YOU WON`);
          player(index);
        } else {
          console.log(`OOPS!🥴 WRONG GUESS ${item}`);
        }
      }
    });

    console.log(`${5 - i} attempts left`);
    i++;
  }
  console.log(
    `OOPS! NO ONE GAVE CORRECT ANSWER SO ${playersArray[id]} IS THE WINNER 💐`
  );
  player(id);
}
